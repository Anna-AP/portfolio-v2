export interface FormationType {
  id: number;
  description_en: string;
  description_fr: string;
  description_it: string;
  label_en: string;
  label_fr: string;
  label_it: string;
  locality: string;
  period_en: string;
  period_it: string;
  period_fr: string;
  school: string;
}

export interface ResumeType {
  id: number;
  activity_en: string;
  activity_fr: string;
  activity_it: string;
  company: string;
  description_en: string;
  description_fr: string;
  description_it: string;
  locality: string;
  period_en: string;
  period_it: string;
  period_fr: string;
}

