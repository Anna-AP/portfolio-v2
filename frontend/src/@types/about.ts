export interface AboutIntroType {
  fr: string;
  en: string;
  it: string;
}