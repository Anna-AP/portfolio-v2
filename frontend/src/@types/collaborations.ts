export interface CollaborationsType {
  id: number;
  image: string;
  label: string;
}
