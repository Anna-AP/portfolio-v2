export interface SkillsType {
  id: number;
  image: string;
  label: string;
}
