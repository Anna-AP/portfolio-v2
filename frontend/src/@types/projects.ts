export interface ProjectType {
  id: number;
  category: string;
  client: string;
  company: string;
  context_en: string;
  context_fr: string;
  context_it: string;
  contributions_en: string;
  contributions_fr: string;
  contributions_it: string;
  images: string[];
  link: string;
  logo: string;
  technos: string;
  title: string;
}
