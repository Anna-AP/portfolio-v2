export interface ContactFormType {
  identity: string;
  mail: string;
  subject: string;
  message: string;
  day?: string;
}