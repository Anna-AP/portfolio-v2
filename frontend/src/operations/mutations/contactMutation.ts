import { useMutation } from 'react-query';

import { ContactFormType } from '@/@types/contact';
import { portfolioService } from '@/services/firebase';

export const useContactMutation = () => {
  return useMutation({
    mutationFn: (data: ContactFormType) => {
      return portfolioService.contact(data);
    },
  });
};
