import { useQuery } from 'react-query';

import { portfolioService } from '@/services/firebase';

export const useAboutInfosQuery = () => {
  return useQuery(['about_infos'], portfolioService.aboutInfos);
};

export const useAboutIntroQuery = () => {
  return useQuery(['about_intro'], portfolioService.aboutIntro);
};

export const useResumesQuery = () => {
  return useQuery(['resumes'], portfolioService.resumes);
};

export const useFormationsQuery = () => {
  return useQuery(['formations'], portfolioService.formations);
};

export const useProjectsQuery = () => {
  return useQuery(['projects'], portfolioService.projects);
};

export const useSkillsQuery = () => {
  return useQuery(['skills'], portfolioService.skills);
};

export const useCollaborationsQuery = () => {
  return useQuery(['collaborations'], portfolioService.collaborations);
};