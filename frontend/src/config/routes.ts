export const routeHome = '/';
export const routeAbout = '/about';
export const routeExperience = '/experience';
export const routeProjects = '/projects';
export const routeContact = '/contact';

export const routeLinkedin = 'https://www.linkedin.com/in/anna-angélique-porte/';
export const routeGitlab = 'https://gitlab.com/users/Anna-AP/projects';