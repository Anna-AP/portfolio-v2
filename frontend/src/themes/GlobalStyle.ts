import { createGlobalStyle } from 'styled-components';

import { fonts } from '@/themes/fonts';

export const GlobalStyle = createGlobalStyle`
  ${fonts}
  
  * {
    margin: 0;
    padding: 0;
  }
  
  body {
    font-family: 'Poppins', sans-serif;
  }
`;