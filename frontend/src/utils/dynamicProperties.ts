export const getLocalizedProperty = <T extends object>(obj: T, baseKey: string, locale: string): string => {
  const key = `${baseKey}_${locale}` as keyof T;
  if (key in obj) {
    return obj[key] as unknown as string;
  }
  return '';
};