import { faHouseChimney, faProjectDiagram, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faUser, faAddressCard } from '@fortawesome/free-regular-svg-icons';
import { useIntl } from 'react-intl';
import { faLinkedin, faGitlab } from '@fortawesome/free-brands-svg-icons';
import { useRouter } from 'next/router';
import Image from 'next/image';

import {
  routeAbout,
  routeContact,
  routeExperience,
  routeGitlab,
  routeHome,
  routeLinkedin,
  routeProjects
} from '@/config/routes';
import photo from '@/assets/img/photo.png';
import {
  StyledFooterCopyright,
  StyledFooterSidebar,
  StyledListContainer,
  StyledListItem,
  StyledNetworksItems,
  StyledSidebarContainer
} from '@/components/Structure/Sidebar/styled';
import Icon from '@/components/Elements/Icon';

const Sidebar = () => {
  const intl = useIntl();
  const router = useRouter();

  const year = new Date().getFullYear();

  const isRouteActive = (route: string) => router.pathname === route;

  return <>
    <StyledSidebarContainer>
      <Image src={photo.src} alt="photo" className="photo" width={199} height={244}/>
      <StyledListContainer>
        <StyledListItem $isActive={isRouteActive(routeHome)}>
          <a href={routeHome}>
            <Icon icon={faHouseChimney} />
            {intl.formatMessage({ id: 'sidebar_link_home' })}
          </a>
        </StyledListItem>
        <StyledListItem $isActive={isRouteActive(routeAbout)}>
          <a href={routeAbout}>
            <Icon icon={faUser} />
            {intl.formatMessage({ id: 'sidebar_link_about' })}
          </a>
        </StyledListItem>
        <StyledListItem $isActive={isRouteActive(routeExperience)}>
          <a href={routeExperience}>
            <Icon icon={faAddressCard} />
            {intl.formatMessage({ id: 'sidebar_link_experience' })}
          </a>
        </StyledListItem>
        <StyledListItem $isActive={isRouteActive(routeProjects)}>
          <a href={routeProjects}>
            <Icon icon={faProjectDiagram} />
            {intl.formatMessage({ id: 'sidebar_link_projects' })}
          </a>
        </StyledListItem>
        <StyledListItem $isActive={isRouteActive(routeContact)}>
          <a href={routeContact}>
            <Icon icon={faEnvelope} />
            {intl.formatMessage({ id: 'sidebar_link_contact' })}
          </a>
        </StyledListItem>
        <StyledListItem>
          <StyledNetworksItems>
            <a href={routeLinkedin}>
              <Icon icon={faLinkedin} />
            </a>
            <a href={routeGitlab}>
              <Icon icon={faGitlab} />
            </a>
          </StyledNetworksItems>
        </StyledListItem>
      </StyledListContainer>
      <StyledFooterSidebar>
        <StyledFooterCopyright>2020 | {year}</StyledFooterCopyright>
        <StyledFooterCopyright>&copy;</StyledFooterCopyright>
        <StyledFooterCopyright>{intl.formatMessage({ id: 'global_name' })}</StyledFooterCopyright>
      </StyledFooterSidebar>
    </StyledSidebarContainer>
  </>;
};

export default Sidebar;