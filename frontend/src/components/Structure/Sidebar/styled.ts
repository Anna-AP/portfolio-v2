import styled from 'styled-components';

interface StyledListItemProps {
  $isActive?: boolean;
}

export const StyledSidebarContainer = styled.div`
  background-color: #2a2b30;
  color: #fff;
  position: relative;
  min-width: 200px;
  max-width: 200px;
  min-height: 100vh;
  
  .photo {
    width: 199px;
    border-right: 1px solid #2a2b30;
  }
`;

export const StyledListContainer = styled.ul`
  padding-left: 0;
  list-style: none;
`;

export const StyledListItem = styled.li<StyledListItemProps>`
  font-size: 12px;
  letter-spacing: 1px;
  text-transform: uppercase;
  border-bottom: 1px solid #202125;
  text-align: left;

  a {
    line-height: 20px;
    padding: 10px;
    padding-left: 70px;
    margin-left: -30px;
    font-size: 1em;
    display: block;
    color: ${(props) => (props.$isActive ? '#00a3e1' : '#999999')};
    font-weight: 500;
    text-decoration: none;
    //transition: all 0.3s;
  }
  
  svg {
    width: 14px;
    height: 14px;
    font-size: 14px;
    margin-right: 15px;
  }
`;

export const StyledNetworksItems = styled.div`
  display: flex;
  justify-content: center;
  cursor: pointer;
`;

export const StyledFooterSidebar = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 30px;
`;

export const StyledFooterCopyright = styled.span`
  font-size: 12px;
  margin-top: 5px;
  color: #999999;
  font-weight: 500;
`;