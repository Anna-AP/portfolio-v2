import { useIntl } from 'react-intl';

import {
  StyledExperienceResume,
  StyledExperienceResumeContainer,
  StyledExperienceResumeTitle,
  StyledExperienceResumeTitleContainer
} from '@/components/Parts/Experience/Resume/styled';
import Timeline from '@/components/Elements/Timeline';
import Spinner from '@/components/Elements/Spinner';
import { useResumesQuery } from '@/operations/queries/useAboutQueries';
import { getLocalizedProperty } from '@/utils/dynamicProperties';

const ExperienceResume = () => {
  const intl = useIntl();

  const { data: resumes, isLoading } = useResumesQuery();

  return <>
    <StyledExperienceResume>
      <StyledExperienceResumeTitleContainer>
        <StyledExperienceResumeTitle>
          {intl.formatMessage({ id: 'experience_resume_title' })}
        </StyledExperienceResumeTitle>
      </StyledExperienceResumeTitleContainer>
      <StyledExperienceResumeContainer>
        {!isLoading && resumes
          ?
          resumes.map((resume, index) => (
            <Timeline
              key={index}
              title={`${getLocalizedProperty(resume, 'period', intl.locale)} &bull; ${getLocalizedProperty(resume, 'activity', intl.locale)}`}
              locality={`${resume.company} ${resume.locality}`}
              description={getLocalizedProperty(resume, 'description', intl.locale)}
            />
          ))
          :
          <Spinner />
        }
      </StyledExperienceResumeContainer>
    </StyledExperienceResume>
  </>;
};

export default ExperienceResume;