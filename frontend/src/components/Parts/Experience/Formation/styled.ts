import styled from 'styled-components';

export const StyledExperienceFormation = styled.div`
  grid-column: span 6;
`;

export const StyledExperienceFormationTitleContainer = styled.div`
  position: relative;
  margin-bottom: 50px;
  text-align: center;
`;

export const StyledExperienceFormationTitle = styled.h3`
  font-family: "RobotoSlab", serif;
  display: inline-block;
  font-size: 22px;
  font-weight: 400;
  line-height: 1em;
  letter-spacing: 0.5px;
  position: relative;
  text-transform: uppercase;
  padding: 5px 10px;
  border-bottom: 2px solid #00a3e1;
`;

export const StyledExperienceFormationContainer = styled.div`
  width: 65%;
  margin: auto;
`;