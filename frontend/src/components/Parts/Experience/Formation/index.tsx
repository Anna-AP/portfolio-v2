import { useIntl } from 'react-intl';

import {
  StyledExperienceFormation,
  StyledExperienceFormationContainer,
  StyledExperienceFormationTitle,
  StyledExperienceFormationTitleContainer
} from '@/components/Parts/Experience/Formation/styled';
import Timeline from '@/components/Elements/Timeline';
import Spinner from '@/components/Elements/Spinner';
import { useFormationsQuery } from '@/operations/queries/useAboutQueries';
import { FormationType } from '@/@types/experiences';
import { getLocalizedProperty } from '@/utils/dynamicProperties';

const ExperienceFormation = () => {
  const intl = useIntl();

  const { data: formations, isLoading } = useFormationsQuery();

  return <>
    <StyledExperienceFormation>
      <StyledExperienceFormationTitleContainer>
        <StyledExperienceFormationTitle>
          {intl.formatMessage({ id: 'experience_formation_title' })}
        </StyledExperienceFormationTitle>
      </StyledExperienceFormationTitleContainer>
      <StyledExperienceFormationContainer>
        {!isLoading && formations
          ?
          formations.map((formation: FormationType, index) => (
            <Timeline
              key={index}
              title={`${getLocalizedProperty(formation, 'period', intl.locale)} &bull; ${getLocalizedProperty(formation, 'label', intl.locale)}`}
              locality={`${formation.school} ${formation.locality}`}
              description={getLocalizedProperty(formation, 'description', intl.locale)}
            />
          ))
          :
          <Spinner />
        }
      </StyledExperienceFormationContainer>
    </StyledExperienceFormation>
  </>;
};

export default ExperienceFormation;