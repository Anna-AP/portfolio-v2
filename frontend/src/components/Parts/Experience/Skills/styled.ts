import styled from 'styled-components';

export const StyledExperienceSkillsContainer = styled.div`
  margin-top: 25px;
  margin-bottom: 40px;
`;

export const StyledExperienceSkillsTitleContainer = styled.div`
  position: relative;
  margin-bottom: 50px;
  text-align: center;
`;

export const StyledExperienceSkillsTitle = styled.h3`
  font-family: "RobotoSlab", serif;
  display: inline-block;
  font-size: 22px;
  font-weight: 400;
  line-height: 1em;
  letter-spacing: 0.5px;
  position: relative;
  text-transform: uppercase;
  padding: 5px 10px;
  border-bottom: 2px solid #00a3e1;
`;