import { useIntl } from 'react-intl';

import {
  StyledExperienceSkillsContainer,
  StyledExperienceSkillsTitle,
  StyledExperienceSkillsTitleContainer
} from '@/components/Parts/Experience/Skills/styled';
import { useSkillsQuery } from '@/operations/queries/useAboutQueries';
import Spinner from '@/components/Elements/Spinner';

import MultiCarousel from '../../../Elements/MultiCarousel';

const ExperienceSkills = () => {
  const intl = useIntl();

  const { data: skills, isLoading } = useSkillsQuery();

  return <>
    <StyledExperienceSkillsContainer>
      <StyledExperienceSkillsTitleContainer>
        <StyledExperienceSkillsTitle>
          {intl.formatMessage({ id: 'experience_skills_title' })}
        </StyledExperienceSkillsTitle>
      </StyledExperienceSkillsTitleContainer>
      {!isLoading && skills ? <MultiCarousel contents={skills} type="skills" /> : <Spinner />}
    </StyledExperienceSkillsContainer>
  </>;
};

export default ExperienceSkills;
