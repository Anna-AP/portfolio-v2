import styled from 'styled-components';

export const StyledAboutGeneralInfos = styled.div`
  grid-column: span 4; 
`;

export const StyledGeneralInfosTitle = styled.h3`
  font-family: "RobotoSlab", serif;
  font-size: 22px;
  font-weight: 500;
  margin-bottom: 10px;
`;

export const StyledGeneralInfosContainer = styled.div`
  height: 450px;
`;

export const StyledGeneralInfosList = styled.ul`
  display: flex;
  flex-direction: column;
  padding-left: 0;
  margin-bottom: 0;
  border-radius: 0.25rem;
`;

export const StyledGeneralInfosItem = styled.li`
  border: none;
  padding: 0.4rem 0.9rem;
  position: relative;
  display: block;
  color: #212529;
  text-decoration: none;
  background-color: #fff;
`;

export const StyledGeneralInfosItemTitle = styled.span`
  font-family: "RobotoSlab", serif;
  font-size: 16px;
  font-weight: 500;
  letter-spacing: 0.5px;
  border-bottom: 2px solid #00a3e1;
  position: relative;
  display: inline-block;
  margin-right: 15px;
  padding-bottom: 2px;
  color: #222;
`;

export const StyledGeneralInfosItemContent = styled.span`
  font-size: 16px;
  color: #595959;
  text-align: justify;
  font-family: "Poppins", serif;
  
  a {
    color: #00a3e1;
    text-decoration: none;
  }
  svg {
    margin-left: 5px;
  }
`;