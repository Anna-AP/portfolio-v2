import { faArrowUpRightFromSquare } from '@fortawesome/free-solid-svg-icons';
import { useIntl } from 'react-intl';

import {
  StyledAboutGeneralInfos, StyledGeneralInfosContainer,
  StyledGeneralInfosItem,
  StyledGeneralInfosItemContent,
  StyledGeneralInfosItemTitle,
  StyledGeneralInfosList,
  StyledGeneralInfosTitle,
} from '@/components/Parts/About/GeneralInfos/styled';
import Icon from '@/components/Elements/Icon';
import Spinner from '@/components/Elements/Spinner';
import { useAboutInfosQuery } from '@/operations/queries/useAboutQueries';

const AboutGeneralInfos = () => {
  const intl = useIntl();

  const { data: about, isLoading } = useAboutInfosQuery();

  const birthDate = new Date(1990, 0, 10);
  const today = new Date();

  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();

  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }

  const infos = [
    {
      titleId: 'about_infos_name',
      content: intl.formatMessage({ id: 'global_name' }),
    },
    {
      titleId: 'about_infos_age',
      content: `${age} ${intl.formatMessage({ id: 'about_infos_count_age' })}`,
    },
    {
      titleId: 'about_infos_locality',
      content: about && about.locality,
    },
    {
      titleId: 'about_infos_activity',
      content: (
        <a href={about && about.activity_link} target="_blank">
          {about && about[`activity_label_${intl.locale}`]}
          <Icon icon={faArrowUpRightFromSquare} />
        </a>
      ),
    },
  ];

  return (
    <StyledAboutGeneralInfos>
      <StyledGeneralInfosTitle>
        {intl.formatMessage({ id: 'about_infos_title' })}
      </StyledGeneralInfosTitle>

      <StyledGeneralInfosContainer>
        {!isLoading ?
          <StyledGeneralInfosList>
            {infos.map(({ titleId, content }) => (
              <StyledGeneralInfosItem key={titleId}>
                <StyledGeneralInfosItemTitle>
                  {intl.formatMessage({ id: titleId })} :
                </StyledGeneralInfosItemTitle>
                <StyledGeneralInfosItemContent>{content}</StyledGeneralInfosItemContent>
              </StyledGeneralInfosItem>
            ))}
          </StyledGeneralInfosList>
          :
          <Spinner />
        }
      </StyledGeneralInfosContainer>
    </StyledAboutGeneralInfos>
  );
};

export default AboutGeneralInfos;
