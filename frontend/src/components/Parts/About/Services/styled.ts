import styled from 'styled-components';

export const StyledAboutServiceContainer = styled.div`
  margin-top: 25px;
`;

export const StyledAboutServiceTitleContainer = styled.div`
  position: relative;
  margin-bottom: 50px;
  text-align: center;
`;

export const StyledAboutServiceTitle = styled.h3`
  font-family: "RobotoSlab", serif;
  display: inline-block;
  font-size: 22px;
  font-weight: 400;
  line-height: 1em;
  letter-spacing: 0.5px;
  position: relative;
  text-transform: uppercase;
  padding: 5px 10px;
  border-bottom: 2px solid #00a3e1;
`;

export const StyledAboutServiceDetails = styled.div`
  display: grid;
  grid-template-columns: repeat(15, 1fr);
  gap: 20px;
`;

export const StyledAboutServiceDetailsItems = styled.div`
  grid-column: span 3;
  display: flex;
  flex-direction: column; 
  align-items: center;
  justify-content: center;
  text-align: center;
  
  h4 {
    font-family: "RobotoSlab", serif;
    display: inline-block;
    font-size: 12px;
    font-weight: 400;
    line-height: 1em;
    letter-spacing: 0.5px;
    position: relative;
    text-transform: uppercase;
    padding: 5px 10px;
    border-bottom: 2px solid #00a3e1;
  }
  
  p {
    font-size: 12px;
    color: #595959;
    text-align: center;
  }
  
  svg {
    font-size: 50px;
    margin-bottom: 10px;
    line-height: 1em;
    color: #00a3e1;
  }
`;

export const StyledAboutServiceDetailsTitle = styled.h4`
  font-family: "RobotoSlab", serif;
  display: inline-block;
  font-size: 12px;
  font-weight: bold;
  line-height: 1em;
  letter-spacing: 0.5px;
  position: relative;
  text-transform: uppercase;
  padding: 5px 10px;
  border-bottom: 2px solid #00a3e1;
`;

export const StyledAboutServiceDetailsContent = styled.p`
  font-size: 12px;
  color: #595959;
  text-align: center;
`;




