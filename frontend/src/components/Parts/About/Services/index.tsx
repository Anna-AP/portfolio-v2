import { useIntl } from 'react-intl';
import {
  faLaptop,
  faRightLeft,
  faChalkboardUser,
} from '@fortawesome/free-solid-svg-icons';
import { faClipboard, faLightbulb } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import {
  StyledAboutServiceContainer,
  StyledAboutServiceDetails,
  StyledAboutServiceDetailsContent,
  StyledAboutServiceDetailsItems,
  StyledAboutServiceDetailsTitle,
  StyledAboutServiceTitle,
  StyledAboutServiceTitleContainer,
} from '@/components/Parts/About/Services/styled';

const AboutServices = () => {
  const intl = useIntl();

  const services = [
    {
      icon: faLaptop,
      subtitleId: 'about_services_subtitle_programmation',
      contentId: 'about_services_content_programmation',
    },
    {
      icon: faClipboard,
      subtitleId: 'about_services_subtitle_management',
      contentId: 'about_services_content_management',
    },
    {
      icon: faRightLeft,
      subtitleId: 'about_services_subtitle_digital_transform',
      contentId: 'about_services_content_digital_transform',
    },
    {
      icon: faLightbulb,
      subtitleId: 'about_services_subtitle_innovation',
      contentId: 'about_services_content_innovation',
    },
    {
      icon: faChalkboardUser,
      subtitleId: 'about_services_subtitle_learning',
      contentId: 'about_services_content_learning',
    },
  ];

  return (
    <StyledAboutServiceContainer>
      <StyledAboutServiceTitleContainer>
        <StyledAboutServiceTitle>
          {intl.formatMessage({ id: 'about_services_title' })}
        </StyledAboutServiceTitle>
      </StyledAboutServiceTitleContainer>
      <StyledAboutServiceDetails>
        {services.map((service) => (
          <StyledAboutServiceDetailsItems key={service.subtitleId}>
            <FontAwesomeIcon icon={service.icon} />
            <StyledAboutServiceDetailsTitle>
              {intl.formatMessage({ id: service.subtitleId })}
            </StyledAboutServiceDetailsTitle>
            <StyledAboutServiceDetailsContent>
              {intl.formatMessage({ id: service.contentId })}
            </StyledAboutServiceDetailsContent>
          </StyledAboutServiceDetailsItems>
        ))}
      </StyledAboutServiceDetails>
    </StyledAboutServiceContainer>
  );
};

export default AboutServices;
