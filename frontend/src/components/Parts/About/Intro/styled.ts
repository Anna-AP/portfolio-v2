import styled from 'styled-components';

export const StyledAboutIntro = styled.div`
  grid-column: span 8;
`;

export const StyledIntroTitle = styled.h3`
  font-family: "RobotoSlab", serif;
  font-size: 22px;
  font-weight: 500;
  margin-bottom: 10px;
`;

export const StyledIntroContent = styled.div`
  font-family: "Poppins", serif;
  font-size: 16px;
  color: #595959;
  text-align: justify;
  font-weight: 300;
  font-style: normal;
  height: 450px;
`;