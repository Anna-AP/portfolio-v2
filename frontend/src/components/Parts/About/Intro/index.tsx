import { useIntl } from 'react-intl';

import {
  StyledAboutIntro,
  StyledIntroContent,
  StyledIntroTitle
} from '@/components/Parts/About/Intro/styled';
import Spinner from '@/components/Elements/Spinner';
import { useAboutIntroQuery } from '@/operations/queries/useAboutQueries';

const AboutIntro = () => {
  const intl = useIntl();

  const { data: about, isLoading } = useAboutIntroQuery();

  return <>
    <StyledAboutIntro>
      <StyledIntroTitle>
        {intl.formatMessage({ id: 'about_intro_title' })}
      </StyledIntroTitle>

      {!isLoading && about ? (
        <StyledIntroContent dangerouslySetInnerHTML={{ __html: about[intl.locale] }} />
      ) : (
        <StyledIntroContent>
          <Spinner />
        </StyledIntroContent>
      )}
    </StyledAboutIntro>
  </>;
};

export default AboutIntro;