import { useIntl } from 'react-intl';

import {
  StyledAboutClientsContainer,
  StyledClientServiceTitle,
  StyledClientServiceTitleContainer
} from '@/components/Parts/About/Collaborations/styled';
import { useCollaborationsQuery } from '@/operations/queries/useAboutQueries';
import Spinner from '@/components/Elements/Spinner';

import MultiCarousel from '../../../Elements/MultiCarousel';


const AboutCollaborations = () => {
  const intl = useIntl();

  const { data: collaborations, isLoading } = useCollaborationsQuery();

  return <>
    <StyledAboutClientsContainer>
      <StyledClientServiceTitleContainer>
        <StyledClientServiceTitle>
          {intl.formatMessage({ id: 'about_collaborations_title' })}
        </StyledClientServiceTitle>
      </StyledClientServiceTitleContainer>
      {!isLoading && collaborations ? <MultiCarousel contents={collaborations} type="collaborations" /> : <Spinner />}
    </StyledAboutClientsContainer>
  </>;
};

export default AboutCollaborations;