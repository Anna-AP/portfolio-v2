import { useIntl } from 'react-intl';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { toast } from 'react-toastify';

import { ContactFormType } from '@/@types/contact';
import { contactValidation } from '@/components/Parts/Contact/Form/validation';
import {
  StyledButtonContainer,
  StyledFieldForm,
  StyledForm,
  StyledRowForm,
} from '@/components/Parts/Contact/Form/styled';
import Quill from '@/components/Elements/Fields/Quill';
import ButtonSubmit from '@/components/Elements/Fields/SubmitButton';
import Input from '@/components/Elements/Fields/Input';
import { useContactMutation } from '@/operations/mutations/contactMutation';


const ContactForm = () => {
  const intl = useIntl();
  const contactMutation = useContactMutation();

  const {
    control,
    handleSubmit,
    reset,
    formState: { errors }
  } = useForm<ContactFormType>({
    resolver: yupResolver(contactValidation),
    defaultValues: {
      identity: '',
      mail: '',
      subject: '',
      message: '',
      day: ''
    }
  });

  const onSubmit = async (formData: ContactFormType) => {
    if(formData.day === '') {
      contactMutation.mutate(formData, {
        onSuccess: () => {
          toast.dismiss();
          toast.success('success');
          reset();
        },
        onError: () => {
          toast.error('error');
        }
      });
    }
  };

  return <>
    <StyledForm onSubmit={handleSubmit(onSubmit)}>
      {/* TODO : gérer l'erreur */}
      {errors && <p>{JSON.stringify(errors)}</p>}
      <StyledRowForm>
        <StyledFieldForm $size="medium">
          <Input
            control={control}
            name="identity"
            label={intl.formatMessage({ id: 'contact_form_identity' })}
            type="text"
          />
        </StyledFieldForm>
        <StyledFieldForm $size="medium">
          <Input
            control={control}
            name="mail"
            label={intl.formatMessage({ id: 'contact_form_mail' })}
            type="email"
          />
        </StyledFieldForm>
      </StyledRowForm>
      <StyledRowForm>
        <StyledFieldForm $size="large">
          <Input
            control={control}
            name="subject"
            label={intl.formatMessage({ id: 'contact_form_subject' })}
            type="text"
          />
        </StyledFieldForm>
      </StyledRowForm>
      <StyledRowForm>
        <StyledFieldForm $size="large">
          <Quill
            control={control}
            name="message"
            label={intl.formatMessage({ id: 'contact_form_message' })}
            defaultValue=""
            error=""
          />
        </StyledFieldForm>
      </StyledRowForm>
      <Input
        control={control}
        name="day"
        type="hidden"
      />
      <StyledButtonContainer>
        <ButtonSubmit label={intl.formatMessage({ id: 'contact_form_save' })} />
      </StyledButtonContainer>
    </StyledForm>
  </>;
};

export default ContactForm;