import styled from 'styled-components';

interface StyledFieldFormType {
  $size: string;
}

export const StyledForm = styled.form`
  width: 80%;
  margin: auto;
`;

export const StyledRowForm = styled.div`
  display: grid;
  grid-template-columns: repeat(12, 1fr);
  gap: 20px;
`;

export const StyledFieldForm = styled.div<StyledFieldFormType>`
  grid-column: ${({ $size }) => $size === 'medium' ? 6 : 12} span;
`;

export const StyledButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

export const StyledErrorContent = styled.p`
  color: red;
  text-align: center;
`;

export const StyledSuccessContent = styled.p`
  color: green;
  text-align: center;
`;