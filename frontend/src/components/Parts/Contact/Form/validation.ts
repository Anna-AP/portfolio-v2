import * as Yup from 'yup';

export const contactValidation = Yup.object().shape({
  identity: Yup.string()
    .required('Le contenu est requis')
    .min(3, 'Doit contenir au moins 3 caractères')
    .max(255, 'Doit contenir au maximum 255 caractères'),
  mail: Yup.string()
    .required('Le contenu est requis')
    .email('Doit être une adresse email valide')
    .min(3, 'Doit contenir au moins 3 caractères')
    .max(255, 'Doit contenir au maximum 255 caractères'),
  subject: Yup.string()
    .required('Le contenu est requis')
    .min(3, 'Doit contenir au moins 3 caractères')
    .max(255, 'Doit contenir au maximum 255 caractères'),
  message: Yup.string()
    .required('Le contenu est requis')
    .min(3, 'Doit contenir au moins 3 caractères')
    .max(255, 'Doit contenir au maximum 255 caractères'),
  day: Yup.string().min(0).max(0),
});
