import styled from 'styled-components';

export const StyledDetailsTitleContainer = styled.div`
  margin: auto;
  text-align: center;
  border-bottom: 1px dashed rgba(0, 0, 0, 0.2);
  width: 80%;
`;

export const StyledDetailsTitle = styled.h4`
  text-transform: uppercase;
  color: #00a3e1;
  font-size: 24px;
  font-weight: 500;
`;

export const StyledCarouselContainer = styled.div`
  margin-top: 20px;
`;

export const StyledDetailsTypeProject = styled.div`
  display: grid;
  grid-template-columns: repeat(12, 1fr);
  gap: 20px;
  width: 80%;
  margin: auto;
  border-bottom: 1px dashed rgba(0, 0, 0, 0.2);
  font-size: 14px;
`;

export const StyledDetailsTypePerso = styled.p`
  grid-column: 12 span;
  font-weight: 500;
  text-transform: uppercase;
  color: #00a3e1;
  text-align: center;
  margin-top: 20px;
  padding-bottom: 15px;
  border-bottom: 1px dashed rgba(0, 0, 0, 0.2);
`;

export const StyledDetailsTypePro = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 20px;
  grid-column: 2 / span 10;
  margin-top: 20px;
  padding-bottom: 15px;
`;

export const StyledDetailsTypeProTexte = styled.p``;

export const StyledDetailsTypeLabel = styled.span`
  font-weight: 500;
  text-transform: uppercase;
  color: #00a3e1;
  font-size: 14px;
`;

export const StyledDetailsTypeOther = styled.div`
  grid-column: 12 span;
  text-align: center;
  margin-top: 20px;
  padding-bottom: 15px;
  border-bottom: 1px dashed rgba(0, 0, 0, 0.2);
`;

export const StyledProjectInfos = styled.div`
  margin-left: 40px;
  margin-top: 10px;
  font-size: 14px;
`;

export const StyledProjectContext = styled.div`
  margin-left: 40px;
  margin-top: 30px;
  margin-right: 40px;
  font-size: 14px;
`;

export const StyledDetailsInfosHTML = styled.span`
  text-align: justify;
`;

export const StyledProjectContribution = styled.div`
  margin-left: 40px;
  margin-top: 30px;
  margin-right: 40px;
  font-size: 14px;
`;