import { useIntl } from 'react-intl';

import SimpleCarousel from '@/components/Elements/SimpleCarousel';
import {
  StyledCarouselContainer,
  StyledDetailsInfosHTML,
  StyledDetailsTitle,
  StyledDetailsTitleContainer,
  StyledDetailsTypeLabel,
  StyledDetailsTypeOther,
  StyledDetailsTypePerso,
  StyledDetailsTypePro,
  StyledDetailsTypeProject,
  StyledDetailsTypeProTexte,
  StyledProjectContext,
  StyledProjectContribution,
  StyledProjectInfos,
} from '@/components/Parts/Projects/ContentModal/styled';
import { getLocalizedProperty } from '@/utils/dynamicProperties';

interface ProjectModalProps {
  content: any;
}

const ProjectModal = ({ content }: ProjectModalProps) => {

  const intl = useIntl();

  return <>
    <StyledDetailsTitleContainer>
      <StyledDetailsTitle>{content.title}</StyledDetailsTitle>
    </StyledDetailsTitleContainer>
    <StyledCarouselContainer>
      {content.images.length > 0 && <SimpleCarousel contents={content.images} />}
    </StyledCarouselContainer>
    <StyledDetailsTypeProject>
      {content.category === 'perso' && <StyledDetailsTypePerso>
        {intl.formatMessage({ id: 'project_modal_type_perso' })}
      </StyledDetailsTypePerso>}
      {content.category === 'pro' && <StyledDetailsTypePro>
        <StyledDetailsTypeProTexte>
          <StyledDetailsTypeLabel>{intl.formatMessage({ id: 'project_modal_type_pro_client' })} : </StyledDetailsTypeLabel>
          {content.client}
        </StyledDetailsTypeProTexte>
        <StyledDetailsTypeProTexte>
          <StyledDetailsTypeLabel>{intl.formatMessage({ id: 'project_modal_type_pro_company' })} : </StyledDetailsTypeLabel>
          {content.company}
        </StyledDetailsTypeProTexte>
      </StyledDetailsTypePro>}
      {content.category === 'other' && <StyledDetailsTypeOther>
        <StyledDetailsTypeProTexte>
          <StyledDetailsTypeLabel>{intl.formatMessage({ id: 'project_modal_type_other_context' })} : </StyledDetailsTypeLabel>
          {content.client}
          {content.client && content.company && ' | '}
          {content.company}
        </StyledDetailsTypeProTexte>
      </StyledDetailsTypeOther>}
    </StyledDetailsTypeProject>
    <StyledProjectInfos>
      <StyledDetailsTypeLabel>{intl.formatMessage({ id: 'project_modal_technos' })} : </StyledDetailsTypeLabel>
      {content.technos}
    </StyledProjectInfos>
    <StyledProjectContext>
      <StyledDetailsTypeLabel>{intl.formatMessage({ id: 'project_modal_context' })}</StyledDetailsTypeLabel>
      <StyledDetailsInfosHTML dangerouslySetInnerHTML={{ __html: getLocalizedProperty(content, 'context', intl.locale) }} />
    </StyledProjectContext>
    {content.contributions_fr && <StyledProjectContribution>
      <StyledDetailsTypeLabel>{intl.formatMessage({ id: 'project_modal_contribution' })}</StyledDetailsTypeLabel>
      <StyledDetailsInfosHTML dangerouslySetInnerHTML={{ __html: getLocalizedProperty(content, 'contributions', intl.locale) }} />
    </StyledProjectContribution>}
  </>;
};

export default ProjectModal;