import { useIntl } from 'react-intl';
import { useState } from 'react';
import Image from 'next/image';

import {
  StyledProjectsFiltersContainer,
  StyledProjectsFiltersListContainer,
  StyledProjectsFiltersListItem,
  StyledProjectsContainer,
  StyledProjectItem
} from '@/components/Parts/Projects/List/styled';
import Modal from '@/components/Elements/Modal';
import Spinner from '@/components/Elements/Spinner';
import ProjectModal from '@/components/Parts/Projects/ContentModal';
import { useProjectsQuery } from '@/operations/queries/useAboutQueries';

const ProjectsDetails = () => {
  const intl = useIntl();
  const [activeFilter, setActiveFilter] = useState<string>('all');
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedProject, setSelectedProject] = useState(null);

  const { data: projects } = useProjectsQuery();

  const handleFilterClick = (filterId: string) => {
    setActiveFilter(filterId);
  };

  const handleProjectClick = (project: any) => {
    setSelectedProject(project);
    setIsModalOpen(true);
  };

  const filters = [
    { label: intl.formatMessage({ id: 'projects_filters_all' }), tag: 'all' },
    { label: intl.formatMessage({ id: 'projects_filters_professionnal' }), tag: 'pro' },
    { label: intl.formatMessage({ id: 'projects_filters_personnal' }), tag: 'perso' },
    { label: intl.formatMessage({ id: 'projects_filters_other' }), tag: 'other' }
  ];

  const filteredProjects = projects && projects.filter(project => activeFilter === 'all' || project.category === activeFilter);

  return <>
    <StyledProjectsFiltersContainer>
      <StyledProjectsFiltersListContainer>
        {filters.map((filter, index) => (
          <StyledProjectsFiltersListItem
            key={index}
            $isActive={activeFilter === filter.tag}
            onClick={() => handleFilterClick(filter.tag)}
          >
            {filter.label}
          </StyledProjectsFiltersListItem>
        ))}
      </StyledProjectsFiltersListContainer>
    </StyledProjectsFiltersContainer>
    <StyledProjectsContainer>
      {filteredProjects
        ?
        filteredProjects.map((project, index) => (
          <StyledProjectItem key={index} onClick={() => handleProjectClick(project)}>
            <Image src={project.logo} alt={project.title} width={170} height={170} />
            <h4>{project.title}</h4>
          </StyledProjectItem>
        ))
        :
        <Spinner />
      }
    </StyledProjectsContainer>

    <Modal isOpen={isModalOpen} onClose={() => setIsModalOpen(false)}>
      <ProjectModal content={selectedProject} />
    </Modal>
  </>;
};

export default ProjectsDetails;