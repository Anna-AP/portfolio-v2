import styled from 'styled-components';

interface StyledProjectsFiltersListItemProps {
  $isActive?: boolean;
}

export const StyledProjectsFiltersContainer = styled.div`
  margin-bottom: 30px;
  text-align: center;
`;

export const StyledProjectsFiltersListContainer = styled.ul`
  display: inline-block;
  margin: 0 auto;
  border-radius: 27px;
  border: 1px dashed rgba(0, 0, 0, 0.2);
  line-height: 0;
  padding-left: 0;

`;

export const StyledProjectsFiltersListItem = styled.li<StyledProjectsFiltersListItemProps>`
  display: inline-block;
  font-size: 16px;
  cursor: pointer;
  font-family: "RobotoSlab", sans-serif;
  position: relative;
  color: ${(props) => (props.$isActive ? '#FBFBFB' : '#555')};
  text-transform: uppercase;
  padding: 20px 20px;
  overflow: hidden;
  -webkit-transition: all 0.3s ease;
  -o-transition: all 0.3s ease;
  transition: all 0.3s ease;
  background-color: ${(props) => (props.$isActive ? '#00a3e1' : 'transparent')};
  border-radius: ${(props) => (props.$isActive ? '27px' : 'none')};
`;

export const StyledProjectsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: start;
`;

export const StyledProjectItem = styled.div`
  width: 200px;
  height: 200px; 
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin: 10px;
  cursor: pointer;
  
  &:hover {
    transform: scale(1.05);
  }
  img {
    max-width: 170px;
    max-height: 170px;
    box-shadow: 3px 2px 5px #595959;
    padding: 10px;
  }
  
  h4 {
    font-size: 12px;
    text-transform: uppercase;
    color: #00a3e1;
    font-weight: 500;
    margin-top: 5px;
    text-align: center;
  }

  opacity: 1;
  transform: scale(1);
  transition: opacity 0.5s ease, transform 0.5s ease;

  &.exit {
    opacity: 0;
    transform: scale(0.5);
  }
`;