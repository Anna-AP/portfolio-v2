import { useIntl } from 'react-intl';

import {
  StyledHomeContainer,
  StyledHomeContent,
  StyledSubtitle,
  StyledTitle
} from '@/components/Sections/HomeContent/styled';

const HomeContent = () => {
  const intl = useIntl();

  return <StyledHomeContainer>
    <StyledHomeContent>
      <StyledTitle>{intl.formatMessage({ id: 'global_name' })}</StyledTitle>
      <StyledSubtitle>{intl.formatMessage({ id: 'global_profession' })}</StyledSubtitle>
    </StyledHomeContent>
  </StyledHomeContainer>;
};

export default HomeContent;