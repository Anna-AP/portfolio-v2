import styled from 'styled-components';

import lyon from '@/assets/img/lyon.jpg';

export const StyledHomeContainer = styled.div`
  width: 100%;
  background-image: url(${lyon.src});
  height: 100vh;
  margin-top: -15px;
`;

export const StyledHomeContent = styled.div`
  transform: translate(0%, 271%);
  background: rgba(255, 255, 255, 0.7);
  top: 45%;
  width: 100%;
  height: 150px;
  text-align: center;
`;

export const StyledTitle = styled.h1`
  text-transform: uppercase;
  font-family: "PlayfairDisplay", serif;
  color: #00a3e1;
  font-weight: bold;
  font-size: 45px;
  margin-top: 15px;
`;

export const StyledSubtitle = styled.h2`
  font-family: "RobotoSlab", serif;
  font-size: 38px;
  font-weight: lighter;
`;