import { useIntl } from 'react-intl';
import { faAddressCard } from '@fortawesome/free-regular-svg-icons';

import {
  StyledExperienceContainer, StyledExperienceInfos,
  StyledExperienceTitle,
  StyledExperienceTitleContainer
} from '@/components/Sections/ExperienceContent/styled';
import ExperienceSkills from '@/components/Parts/Experience/Skills';
import ExperienceResume from '@/components/Parts/Experience/Resume';
import ExperienceFormation from '@/components/Parts/Experience/Formation';
import Icon from '@/components/Elements/Icon';

const ExperienceContent = () => {
  const intl = useIntl();

  return <StyledExperienceContainer>
    <StyledExperienceTitleContainer>
      <StyledExperienceTitle>
        {intl.formatMessage({ id: 'sidebar_link_experience' })}
        <Icon icon={faAddressCard} />
      </StyledExperienceTitle>
    </StyledExperienceTitleContainer>
    <ExperienceSkills />
    <StyledExperienceInfos>
      <ExperienceResume />
      <ExperienceFormation />
    </StyledExperienceInfos>
  </StyledExperienceContainer>;
};

export default ExperienceContent;