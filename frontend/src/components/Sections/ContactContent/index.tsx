import { useIntl } from 'react-intl';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';

import Icon from '@/components/Elements/Icon';
import {
  StyledContactContainer,
  StyledContactTitle,
  StyledContactTitleContainer
} from '@/components/Sections/ContactContent/styled';
import ContactForm from '@/components/Parts/Contact/Form';

const ContactContent = () => {
  const intl = useIntl();

  return <>
    <StyledContactContainer>
      <StyledContactTitleContainer>
        <StyledContactTitle>
          {intl.formatMessage({ id: 'sidebar_link_contact' })}
          <Icon icon={faEnvelope} />
        </StyledContactTitle>
      </StyledContactTitleContainer>
      <ContactForm />
    </StyledContactContainer>
  </>;
};

export default ContactContent;