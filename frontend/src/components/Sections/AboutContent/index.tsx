import { useIntl } from 'react-intl';
import { faUser } from '@fortawesome/free-regular-svg-icons';

import {
  StyledAboutContainer, StyledAboutInfos,
  StyledAboutTitle,
  StyledAboutTitleContainer
} from '@/components/Sections/AboutContent/styled';
import AboutIntro from '@/components/Parts/About/Intro';
import AboutGeneralInfos from '@/components/Parts/About/GeneralInfos';
import AboutServices from '@/components/Parts/About/Services';
import Icon from '@/components/Elements/Icon';

import AboutCollaborations from '../../Parts/About/Collaborations';

const AboutContent = () => {
  const intl = useIntl();

  return <StyledAboutContainer>
    <StyledAboutTitleContainer>
      <StyledAboutTitle>
        {intl.formatMessage({ id: 'sidebar_link_about' })}
        <Icon icon={faUser} />
      </StyledAboutTitle>
    </StyledAboutTitleContainer>
    <StyledAboutInfos>
      <AboutIntro />
      <AboutGeneralInfos />
    </StyledAboutInfos>
    <AboutServices />
    <AboutCollaborations />
  </StyledAboutContainer>;
};

export default AboutContent;