import styled from 'styled-components';

export const StyledProjectsContainer = styled.div`
  max-width: 1320px;
  margin: auto;
`;

export const StyledProjectsTitleContainer = styled.div`
  margin-bottom: 50px;
  margin-top: 50px;
  border-bottom: 1px dashed rgba(0, 0, 0, 0.2);
  padding-bottom: 10px;
`;

export const StyledProjectsTitle = styled.div`
  width: 100%;
  font-size: 36px;
  font-weight: 700;
  letter-spacing: 1.5px;
  line-height: 1em;
  text-transform: capitalize;
  position: relative;
  display: inline-block;
  font-family: "PlayfairDisplay", serif;
  color: #00a3e1;
  
  svg {
    float: right;
    width: 38px;
    line-height: 1em;
    color: #00a3e1;
  }
`;