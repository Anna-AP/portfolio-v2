import { useIntl } from 'react-intl';
import { faProjectDiagram } from '@fortawesome/free-solid-svg-icons';

import {
  StyledProjectsContainer,
  StyledProjectsTitle,
  StyledProjectsTitleContainer
} from '@/components/Sections/ProjectsContent/styled';
import ProjectsDetails from '@/components/Parts/Projects/List';
import Icon from '@/components/Elements/Icon';

const ProjectsContent = () => {
  const intl = useIntl();

  return <>
    <StyledProjectsContainer>
      <StyledProjectsTitleContainer>
        <StyledProjectsTitle>
          {intl.formatMessage({ id: 'sidebar_link_projects' })}
          <Icon icon={faProjectDiagram} />
        </StyledProjectsTitle>
      </StyledProjectsTitleContainer>
      <ProjectsDetails />
    </StyledProjectsContainer>
  </>;
};

export default ProjectsContent;