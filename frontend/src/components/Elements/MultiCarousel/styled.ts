import styled from 'styled-components';

export const StyledCarouselCollaborationsImg = styled.img`
  opacity: 0.7;
  transition: opacity .4s ease;
  
  &:hover {
    opacity: 1;
    transition: opacity .4s ease;
  }
`;

export const StyledCarouselSkillsImg = styled.img`
  opacity: 0.7;
  transition: opacity .4s ease;
  width: 50px;
  filter: grayscale(1);
  cursor: pointer;

  &:hover {
    opacity: 1;
    transition: opacity .4s ease;
    filter: grayscale(0);
  }
`;
