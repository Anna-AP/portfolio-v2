import Carousel from 'react-multi-carousel';

import 'react-multi-carousel/lib/styles.css';
import {
  StyledCarouselCollaborationsImg,
  StyledCarouselSkillsImg
} from '@/components/Elements/MultiCarousel/styled';

interface CarouselType {
  contents: {
    image: string;
    label: string;
  }[];
  type: 'collaborations'|'skills'|'projects';
}

const MultiCarousel = ({ contents, type }: CarouselType) => {

  const responsive = {
    superLargeDesktop: {
      breakpoint: { max: 4000, min: 3000 },
      items: type === 'skills' ? 10 : 6
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items:  type === 'skills' ? 10 : 6
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 4
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1
    }
  };

  return <>
    <Carousel responsive={responsive} autoPlay={true} autoPlaySpeed={1500} infinite={true} arrows={false}>
      {contents.map((content, index) => (
        <div key={index}>
          {type === 'skills' && <StyledCarouselSkillsImg src={content.image} alt={content.label} title={content.label} />}
          {type === 'collaborations' && <StyledCarouselCollaborationsImg src={content.image} alt={content.label} title={content.label} />}
        </div>
      ))}
    </Carousel>
  </>;
};

export default MultiCarousel;