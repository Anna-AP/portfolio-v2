import styled from 'styled-components';

export const InputContainer = styled.div`
  margin-bottom: 20px;
`;

export const InputLabel = styled.label`
  display: block;
  font-weight: 400;
  font-family: "RobotoSlab", serif;
  color: #575757;
`;

export const ErrorText = styled.div`
  color: red;
  margin-top: 8px;
`;

export const InputHtml = styled.input`
  display: block;
  width: 100%;
  font-size: 1rem;
  font-weight: 400;
  line-height: 1.5;
  color: #212529;
  background-color: #fff;
  background-clip: padding-box;
  appearance: none;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  border: 1px solid #ced4da;
  border-radius: 0.2rem;
  padding: 0.25rem 0.5rem;
`;