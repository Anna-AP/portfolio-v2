import { Control, Controller } from 'react-hook-form';

import { ErrorText, InputContainer, InputHtml, InputLabel } from '@/components/Elements/Fields/Input/styled';

interface InputProps {
  control: Control<any>;
  name: string;
  label?: string;
  type?: string;
  defaultValue?: string;
  error?: string | null;
}

const Input = ({ control, name, label, type = 'text', defaultValue = '', error }: InputProps) => {
  return (
    <InputContainer>
      {type !== 'hidden' && <InputLabel htmlFor={name}>{label}</InputLabel>}
      <Controller
        name={name}
        control={control}
        defaultValue={defaultValue}
        render={({ field }) => (
          <InputHtml
            id={name}
            type={type}
            value={field.value}
            onChange={field.onChange}
            onBlur={field.onBlur}
          />
        )}
      />
      {error && <ErrorText>{error}</ErrorText>}
    </InputContainer>
  );
};

export default Input;
