import { StyledButton } from '@/components/Elements/Fields/SubmitButton/styled';

interface ButtonSubmitProps {
  label: string;
  disable?: boolean;
};

const ButtonSubmit = ({ label, disable }: ButtonSubmitProps) => {
  return <StyledButton disabled={disable}>{label}</StyledButton>;
};

export default ButtonSubmit;
