import styled from 'styled-components';

export const StyledButton = styled.button`
  border: 2px solid #00a3e1;
  background-color: #ffffff;
  color: #00a3e1;
  text-transform: uppercase;
  font-weight: bolder;
  width: 200px;
  height: 30px;
  cursor: pointer;
`;