import dynamic from 'next/dynamic';
import { Control,Controller } from 'react-hook-form';

import { QuillContainer, QuillLabel } from '@/components/Elements/Fields/Quill/styled';
import 'react-quill/dist/quill.snow.css';

const ReactQuill = dynamic(() => import('react-quill'), {
  ssr: false,
});

const modules = {
  toolbar: [
    ['bold', 'italic', 'underline', 'strike'],
    [{ list: 'ordered' }, { list: 'bullet' }],
    ['link'],
    ['clean']
  ],
};

const formats = ['bold', 'italic', 'underline', 'strike', 'list', 'bullet', 'link'];

interface QuillProps {
  control: Control<any>;
  name: string;
  label: string;
  defaultValue?: string;
  error?: string | null;
}

const Quill = ({ control, name, label, defaultValue = '', error }: QuillProps) => {
  return <>
    <QuillContainer>
      <QuillLabel>{label}</QuillLabel>
      <Controller
        name={name}
        control={control}
        defaultValue={defaultValue}
        render={({ field }) => (
          <ReactQuill
            value={field.value}
            theme="snow"
            modules={modules}
            formats={formats}
            onChange={(content, delta, source, editor) => field.onChange(editor.getHTML())}
          />
        )}
      />
      {error && <div>{error}</div>}
    </QuillContainer>
  </>;
};

export default Quill;