import styled from 'styled-components';

export const QuillContainer = styled.div`
  margin-bottom: 20px;
  
  .quill {
    width: 102%;
  }

  .ql-editor {
    height: 200px;
  }
`;

export const QuillLabel = styled.label`
  display: block;
  font-weight: 400;
  font-family: "RobotoSlab", serif;
  color: #575757;
`;