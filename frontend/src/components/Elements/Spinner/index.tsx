import { StyledLoadingSpinner } from '@/components/Elements/Spinner/styled';

const Spinner = () => {
  return <>
    <StyledLoadingSpinner>
      <span className="loader"></span>
    </StyledLoadingSpinner>
  </>;
};

export default Spinner;
