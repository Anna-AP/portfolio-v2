import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

interface IconProps {
  icon: any;
}

const Icon = ({ icon }: IconProps) => {

  return <FontAwesomeIcon icon={icon} width={14} height={14} />;
};

export default Icon;