import styled from 'styled-components';

export const TimelineContainer = styled.div`
  position: relative;
  overflow: hidden;
  
  &:before {
    content: "";
    position: absolute;
    top: 0;
    left: 8px;
    margin-left: -2px;
    width: 2px;
    height: 100%;
    background: #00a3e1;
    z-index: 1;
  }  
`;

export const TimelineStructure = styled.div`
  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: flex;
  -webkit-box-pack: justify;
  -webkit-justify-content: space-between;
  -moz-box-pack: justify;
  justify-content: space-between;
  clear: both;
`;

export const TimelineMarker = styled.div`
  width: 7px;
  height: 6px;
  border-radius: 50%;
  border: 2px solid #00a3e1;
  background: #ffffff;
  z-index: 9999;
  margin-left: 2px;
`;

export const TimelineContent = styled.div`
  width: 95%;
  padding: 0 15px;
  color: #666;
`;

export const TimelineTitle = styled.h3`
  font-family: "RobotoSlab", serif;
  font-size: 14px;
  color: #000000;
`;

export const TimelineDetails = styled.p`
  line-height: 1.5em;
  word-spacing: 1px;
  margin: 0;
  font-size: 12px !important;
  text-transform: uppercase !important;
  font-weight: 500;
  color: #00a3e1 !important;
  text-align: justify;
`;

export const TimelineDescriptions = styled.div`
  font-size: 13px;
`;

export const TimelineBottomMarker = styled.div`
  width: 6px;
  height: 6px;
  border-radius: 50%;
  border: 2px solid #00a3e1;
  background: #ffffff;
  z-index: 9999;
  position: absolute;
  left: 2px;
  bottom: 0;
`;