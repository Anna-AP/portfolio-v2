import React from 'react';

import {
  TimelineBottomMarker,
  TimelineContainer,
  TimelineContent,
  TimelineDescriptions,
  TimelineDetails,
  TimelineMarker,
  TimelineStructure,
  TimelineTitle
} from '@/components/Elements/Timeline/styled';

interface TimelineType {
  title: string|React.ReactElement;
  locality: string;
  description: string|React.ReactElement;
}

const Timeline = ({ title, locality, description }: TimelineType) => {

  return <>
    <TimelineContainer>
      <TimelineStructure>
        <TimelineMarker />
        <TimelineContent>
          <TimelineTitle dangerouslySetInnerHTML={{ __html: title }} />
          <TimelineDetails>{locality}</TimelineDetails>
          <TimelineDescriptions dangerouslySetInnerHTML={{ __html: description }} />
        </TimelineContent>
        <TimelineBottomMarker />
      </TimelineStructure>
    </TimelineContainer>
    <br />
  </>;
};

export default Timeline;