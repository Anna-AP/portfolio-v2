import styled from 'styled-components';

export const StyledSimpleCarouselContainer = styled.div`
  width: 665px;
  margin: auto;
  
  .react-multi-carousel-list {
    box-shadow: 2px 2px 5px #595959;
  }
  
  .react-multi-carousel-track {
    height: 338px;
  }
`;

export const StyledCarouselProjectsImg = styled.img`
  width: 100%;
`;