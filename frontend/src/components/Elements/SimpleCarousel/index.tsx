import Carousel from 'react-multi-carousel';

import 'react-multi-carousel/lib/styles.css';
import { StyledCarouselProjectsImg, StyledSimpleCarouselContainer } from '@/components/Elements/SimpleCarousel/styled';


interface CarouselType {
  contents: {
    image: string;
    label: string;
  }[];
}

const SimpleCarousel = ({ contents }: CarouselType) => {

  const responsive = {
    superLargeDesktop: {
      breakpoint: { max: 4000, min: 3000 },
      items: 1
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 1
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1
    }
  };

  return <>
    <StyledSimpleCarouselContainer>
      <Carousel responsive={responsive} autoPlay={contents.length > 1} autoPlaySpeed={1500} arrows={false} >
        {contents.map((content, index) => (
          <div key={index}>
            <StyledCarouselProjectsImg src={content.image} alt={content.label} title={content.label} />
          </div>
        ))}
      </Carousel>
    </StyledSimpleCarouselContainer>
  </>;
};

export default SimpleCarousel;