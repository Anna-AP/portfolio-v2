import React, { useState, useEffect } from 'react';

import { StyledModalContainer, StyledModalContent } from '@/components/Elements/Modal/styled';

interface ModalProps {
  isOpen: boolean;
  onClose: () => void;
  children: React.ReactNode;
}

const Modal = ({ isOpen, onClose, children }: ModalProps) => {
  const [shouldRender, setShouldRender] = useState<boolean>(isOpen);

  useEffect(() => {
    let timeoutId: NodeJS.Timeout;

    if (isOpen) {
      setShouldRender(true);
    } else {
      timeoutId = setTimeout(() => {
        setShouldRender(false);
      }, 300);
    }

    return () => clearTimeout(timeoutId);
  }, [isOpen]);

  if (!shouldRender) return null;

  return (
    <StyledModalContainer $isOpen={isOpen} onClick={onClose} onAnimationEnd={() => !isOpen && setShouldRender(false)}>
      <StyledModalContent $isOpen={isOpen} onClick={(e: React.MouseEvent) => e.stopPropagation()}>
        {children}
      </StyledModalContent>
    </StyledModalContainer>
  );
};

export default Modal;

