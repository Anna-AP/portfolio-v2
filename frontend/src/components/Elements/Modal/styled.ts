import styled, { keyframes } from 'styled-components';

interface StyledModalContainerType {
  $isOpen: boolean;
}

interface StyledModalContentType {
  $isOpen: boolean;
}


const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

const fadeOut = keyframes`
  from {
    opacity: 1;
  }
  to {
    opacity: 0;
  }
`;

const slideInFromTop = keyframes`
  from {
    transform: translateY(-100%);
    opacity: 0;
  }
  to {
    transform: translateY(0);
    opacity: 1;
  }
`;

const slideOutToTop = keyframes`
  from {
    transform: translateY(0);
    opacity: 1;
  }
  to {
    transform: translateY(-100%);
    opacity: 0;
  }
`;

export const StyledModalContainer = styled.div<StyledModalContainerType>`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5);
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 9999;
  animation: ${({ $isOpen }) => $isOpen ? fadeIn : fadeOut} 0.5s ease-out
`;

export const StyledModalContent = styled.div<StyledModalContentType>`
  position: relative;
  display: flex;
  flex-direction: column;
  width: 800px;
  pointer-events: auto;
  background-clip: padding-box;
  border: 1px solid rgba(0, 0, 0, 0.2);
  outline: 0;
  background-color: #fff;
  padding: 20px;
  border-radius: 0.3rem;
  animation: ${({ $isOpen }) => $isOpen ? slideInFromTop : slideOutToTop} 0.5s ease-out;
  max-height: 90%;
  overflow-y: auto;

  &::-webkit-scrollbar {
    width: 3px;
  }
  &::-webkit-scrollbar-track {
    background: #f1f1f1;
  }
  &::-webkit-scrollbar-thumb {
    background: #888;
  }
  &::-webkit-scrollbar-thumb:hover {
    background: #555;
  }
`;
