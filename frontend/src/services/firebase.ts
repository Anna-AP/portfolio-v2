import { collection, doc, getDoc, getDocs, setDoc } from 'firebase/firestore';

import { FormationType, ResumeType } from '@/@types/experiences';
import { ProjectType } from '@/@types/projects';
import { ContactFormType } from '@/@types/contact';
import { SkillsType } from '@/@types/skills';
import { CollaborationsType } from '@/@types/collaborations';

import { db } from '../../firebase';

const aboutIntro = async () => {
  const intro = await getDoc(doc(db, 'app/portfolio/about', 'intro'));
  return intro.exists() ? intro.data() : {};
};

const aboutInfos = async () => {
  const infos = await getDoc(doc(db, 'app/portfolio/about', 'infos'));
  return infos.exists() ? infos.data() : {};
};

const formations = async () =>  {
  const formationsSnap  = await getDocs(collection(db, 'app/portfolio/formations'));
  const formations = formationsSnap.docs.map(doc => ({ id: parseInt(doc.id), ...doc.data() } as FormationType));
  formations.sort((a, b) => a.id - b.id);
  if (formations.length > 0) {
    return formations;
  }
};

const resumes = async () => {
  const resumesSnap  = await getDocs(collection(db, 'app/portfolio/resumes'));
  const resumes = resumesSnap.docs.map(doc => ({ id: parseInt(doc.id), ...doc.data() } as ResumeType));
  resumes.sort((a, b) => a.id - b.id);
  if (resumes.length > 0) {
    return resumes;
  }
};

const projects = async () => {
  const projectsSnap  = await getDocs(collection(db, 'app/portfolio/projects'));
  const projects = projectsSnap.docs.map(doc => ({ id: parseInt(doc.id), ...doc.data() } as ProjectType));
  projects.sort((a, b) => a.id - b.id);
  if (projects.length > 0) {
    return projects;
  }
};

const skills = async () => {
  const skillsSnap  = await getDocs(collection(db, 'app/portfolio/skills'));
  const skills = skillsSnap.docs.map(doc => ({ id: parseInt(doc.id), ...doc.data() } as SkillsType));
  skills.sort((a, b) => a.id - b.id);
  if (skills.length > 0) {
    return skills;
  }
};

const collaborations = async () => {
  const collaborationsSnap  = await getDocs(collection(db, 'app/portfolio/collaborations'));
  const collaborations = collaborationsSnap.docs.map(doc => ({ id: parseInt(doc.id), ...doc.data() } as CollaborationsType));
  collaborations.sort((a, b) => a.id - b.id);
  if (collaborations.length > 0) {
    return collaborations;
  }
};

const contact = async (data: ContactFormType) => {
  const documentId = Date.now().toString();
  const docRef = doc(db, 'app/portfolio/contacts', documentId);
  try {
    await setDoc(docRef, {
      identity: data.identity,
      mail: data.mail,
      subject: data.subject,
      message: data.message,
    });
    return { success: true };
  } catch (e) {
    return { success: false, error: e };
  }
};

export const portfolioService = {
  aboutIntro,
  aboutInfos,
  formations,
  resumes,
  projects,
  skills,
  collaborations,
  contact
};