import styled from 'styled-components';

export const StyledLayoutContainer = styled.div`
  display: flex;
`;

export const StyledContentContainer = styled.div`
  width: 100%;
`;