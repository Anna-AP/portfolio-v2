import React from 'react';
import { ThemeProvider } from 'styled-components';

import Sidebar from '@/components/Structure/Sidebar/sidebar';
import * as themes from '@/themes';
import { GlobalStyle } from '@/themes/GlobalStyle';
import { StyledContentContainer, StyledLayoutContainer } from '@/layouts/styled';

interface LayoutType {
  children: React.ReactNode;
}

const Layout = ({ children }: LayoutType) => {

  return <ThemeProvider theme={themes['defaultTheme']}>
    <GlobalStyle />
    <StyledLayoutContainer>
      <Sidebar />
      <StyledContentContainer>
        {children}
      </StyledContentContainer>
    </StyledLayoutContainer>
  </ThemeProvider>;
};

export default Layout;