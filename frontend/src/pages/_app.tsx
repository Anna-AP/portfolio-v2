import { AppProps } from 'next/app';
import { IntlProvider } from 'react-intl';
import React, { useState, useEffect } from 'react';
import {  QueryClientProvider } from 'react-query';
import { Slide, ToastContainer } from 'react-toastify';

import { queryClient } from '../../query';
import defaultMessages from '../translations/fr.json';
import 'react-toastify/dist/ReactToastify.css';

interface Messages {
  [key: string]: string;
}

function MyApp({ Component, pageProps }: AppProps) {
  const [locale] = useState<string>('fr');
  const [messages, setMessages] = useState<Messages>(defaultMessages);

  useEffect(() => {
    import(`./../translations/${locale}.json`)
      .then((messages: { default: Messages }) => {
        setMessages(messages.default);
      })
      .catch((error) => console.error(`Could not load ${locale} messages`, error));
  }, [locale]);

  return (
    <QueryClientProvider client={queryClient}>
      <IntlProvider locale={locale} messages={messages}>
        <Component {...pageProps} />
      </IntlProvider>
      <ToastContainer
        position="bottom-right"
        autoClose={5000}
        hideProgressBar
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
        transition={Slide}
      />
    </QueryClientProvider>
  );
}

export default MyApp;
