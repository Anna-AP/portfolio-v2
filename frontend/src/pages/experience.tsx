import Layout from '@/layouts/layout';
import ExperienceContent from '@/components/Sections/ExperienceContent';

const Experience = () => {
  return <Layout>
    <ExperienceContent />
  </Layout>;
};

export default Experience;