import Layout from '@/layouts/layout';
import ProjectsContent from '@/components/Sections/ProjectsContent';

const Projects = () => {
  return <Layout>
    <ProjectsContent />
  </Layout>;
};

export default Projects;