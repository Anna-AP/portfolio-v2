import Layout from '@/layouts/layout';
import ContactContent from '@/components/Sections/ContactContent';

const Contact = () => {
  return <Layout>
    <ContactContent />
  </Layout>;
};

export default Contact;