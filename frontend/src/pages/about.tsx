import Layout from '@/layouts/layout';
import AboutContent from '@/components/Sections/AboutContent';

const About = () => {
  return <Layout>
    <AboutContent />
  </Layout>;
};

export default About;