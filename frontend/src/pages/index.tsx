import Layout from '@/layouts/layout';
import HomeContent from '@/components/Sections/HomeContent';

const Home = () => {
  return <Layout>
    <HomeContent />
  </Layout>;
};

export default Home;